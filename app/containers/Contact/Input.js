import styled from 'styled-components';

const Input = styled.input`
  outline: none;
  width: 100%;
  border-bottom: 1px solid grey;
  &:focus, &:hover {
    outline: none;
  }
`;

export default Input;
