import styled from 'styled-components';

const Textarea = styled.textarea`
  width: 120%;
  box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.75);
  display: block;
  @media(max-width: 940px){
    width: 100%;
  }
`;

export default Textarea;
