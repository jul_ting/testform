/**
 *
 * Contact
 *
 */

import React from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { compose } from 'redux';
import { Column, Row } from 'hedron';
import axios from 'axios';
import Input from './Input';
import Textarea from './Textarea';
import Button from './Button';

class Contact extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      name: '',
      phoneNumber: '',
      message: '',
      companyName: '',
      button: false,
      redirect: false,
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  onSubmit(e) {
    this.setState({ ...this.state, button: true });
    e.preventDefault();
    const name = this.state.name;
    const email = this.state.email;
    const message = this.state.message;
    const phoneNumber = this.state.phoneNumber;
    const companyName = this.state.companyName;
    const _subject = 'Email from foobar Contact Form'; // eslint-disable-line
    const _cc = 'jul_ting@yahoo.com'; // eslint-disable-line

    axios.post('https://formspree.io/julius.ting.work@gmail.com', { // you can change the email
      name,
      email,
      phoneNumber,
      companyName,
      message,
      _subject,
      _cc,
    })
      .then((response) => {
        console.log(response); // eslint-disable-line
        this.setState({ ...this.state, redirect: true });
      })
      .catch((error) => {
        alert(error); // eslint-disable-line
        this.setState({ ...this.state, button: false });
      });
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  render() {
    if (this.state.redirect) {
      return (
        <Redirect to="/thankyou" /> // will do the redirect
      );
    }
    return (
      <div>
        <Row>
          <Column md={12}>
            <h1>Contact Us</h1>
          </Column>
        </Row>
        <Row>
          <Column md={12}>
            <form onSubmit={this.onSubmit}>
              <Row>
                <Column md={4}>
                  Email<span style={{ color: 'red' }}>*</span>:
                </Column>
                <Column md={8}>
                  <Input type="email" name="email" onChange={this.handleChange} value={this.state.email}
                         placeholder="Your Email" required />
                </Column>
              </Row>
              <Row>
                <Column md={4}>
                  Name<span style={{ color: 'red' }}>*</span>:
                </Column>
                <Column md={8}>
                  <Input type="name" name="name" onChange={this.handleChange} value={this.state.name}
                         placeholder="Your Name" required />
                </Column>
              </Row>
              <Row>
                <Column md={4}>
                  Phone Number:
                </Column>
                <Column md={8}>
                  <Input type="phone_number" name="phoneNumber" onChange={this.handleChange}
                         value={this.state.phoneNumber} placeholder="Your phone number" />
                </Column>
              </Row>
              <Row>
                <Column md={4}>
                  Company Name:
                </Column>
                <Column md={8}>
                  <Input type="company_name" name="companyName" onChange={this.handleChange}
                         value={this.state.companyName} placeholder="Company Name" />
                </Column>
              </Row>
              <Row>
                <Column md={4}>
                  Message<span style={{ color: 'red' }}>*</span>:
                </Column>
                <Column md={8}>
                  <div>
                    <Textarea required name="message" onChange={this.handleChange} value={this.state.message}
                              placeholder="Ask us anything or request for a demo." rows="18" />
                  </div>
                </Column>
              </Row>
              <Row justifyContent="flex-end">
                <Button type="submit" disabled={this.state.button}>Submit</Button>
              </Row>
            </form>
          </Column>
        </Row>
      </div>
    );
  }
}

Contact.propTypes = {};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(null, mapDispatchToProps);

export default compose(
  withConnect,
)(Contact);
