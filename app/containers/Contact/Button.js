import styled from 'styled-components';

const Button = styled.button`
  width: 200px;
  height: 50px;
  font-size: 24px;
  font-weight: bold;
  background-color: #fce02d;
  border: 2px solid blue;
  
  &:active {
    background-color: #f7d200;
    outline: none;
  }
`;

export default Button;
